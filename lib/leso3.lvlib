﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="12008004">
	<Property Name="NI.Lib.Icon" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,*!!!*Q(C=X:5B=&amp;."%)&lt;`-)D%*7A%T;3?G;W!K;C*DIP$16=T2821+99*KNA6%3!D17;G.G;H+/JKE&lt;&amp;5-00YXO-3"K;*9B#^SVZ?`LX&gt;_^\NT56K7FO[L\P@$KP,1?OYKKJ:_X.6H;B4V?."68U&gt;.0\7]U%TN&amp;`&lt;3*W4^HSAVPF*U4@_YWL*=$83!3ZV:C3Y`#._@V)`(]`7@OFANP;XHOUT4/L]S`;HE6IP2L`^B`"]?=?QB)G='\[3`Y[U5I&lt;[4;^'N`D8:7C^(82/&gt;PH0VY8&lt;(L_]X@_L$&amp;P]J1R&lt;_?ZO/^PF0+IO&lt;I[K[C_\O.E;-0FRO_VM,9HNRR),T$($U"-^U2-^U2-^U1-^U!-^U!-^U"X&gt;U2X&gt;U2X&gt;U1X&gt;U!X&gt;U!X&gt;U*O/,H3B#RWE:0&amp;EI32JEC#:$)K3)?&amp;*?"+?B)?@3HA3HI1HY5FYG++%*_&amp;*?"+?B)=Q*4Q*4]+4]#1]J'J)MOHI]#1]J&amp;@!%`!%0!&amp;0Q-/3#HA#A'#R)('1"!Q&amp;TO!BY!FY!BY?&amp;@!%0!&amp;0Q"0QY&amp;&lt;!%`!%0!&amp;0Q%.)MSP2U.1&gt;(2\3S/&amp;R?"Q?B]@B)&lt;5=(I@(Y8&amp;Y("[7E]0D]$A1TI*/=B$E"$E4H"]/D]0$FRQ?B]@B=8A=(FR.B&lt;T:G:KG\ODQ'$Q'D]&amp;D]"A]J*$"9`!90!;0Q5.;'4Q'D]&amp;D]"A],#7$R_!R?!Q19V'7FZ(-#$1G'9,"Q[=Z,&gt;:5+2I3;\J5$K`+I61Z&lt;#K(3/6QK"2&gt;J:AK26,:@*6.6&gt;EMF5V1?4E6;"59F565AOO*7D&amp;?9QNMDEWR5WS-$&lt;%_VKN$``(%V7KF[_NL,29,T?&gt;T4;&gt;4H:[?;DQ?;TA=KN`PK^@L&lt;;["2`4.B&lt;$\8HL[`?(,\FG`W`X7?NT^]+&lt;^Y0X(^B(;VI"88&amp;F\J.^\AP&amp;8M9^.NF^DJ?VE_$^XI_YV&gt;_/[M5=`!&gt;2V`[]!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">302022660</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI_IconEditor" Type="Str">49 50 48 48 56 48 50 52 13 0 0 0 0 1 23 21 76 111 97 100 32 38 32 85 110 108 111 97 100 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 9 0 0 0 80 1 100 1 100 80 84 72 48 0 0 0 4 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 5 76 69 83 79 51 0 0 0 3 108 105 98 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 7 83 121 108 102 97 101 110 0 1 12 1 1

</Property>
	<Item Name="dll" Type="Folder"/>
	<Item Name="help" Type="Folder">
		<Item Name="cbool.png" Type="Document" URL="../help/cbool.png"/>
		<Item Name="cerrcodeclst.png" Type="Document" URL="../help/cerrcodeclst.png"/>
		<Item Name="ci32.png" Type="Document" URL="../help/ci32.png"/>
		<Item Name="cstr.png" Type="Document" URL="../help/cstr.png"/>
		<Item Name="ibool.png" Type="Document" URL="../help/ibool.png"/>
		<Item Name="ierrcodeclst.png" Type="Document" URL="../help/ierrcodeclst.png"/>
		<Item Name="ii32.png" Type="Document" URL="../help/ii32.png"/>
		<Item Name="istr.png" Type="Document" URL="../help/istr.png"/>
		<Item Name="leso3_lvlib_leso3Open.html" Type="Document" URL="../help/leso3_lvlib_leso3Open.html"/>
		<Item Name="leso3_lvlib_leso3Openc.png" Type="Document" URL="../help/leso3_lvlib_leso3Openc.png"/>
	</Item>
	<Item Name="subVI" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="error.vi" Type="VI" URL="../subVI/error.vi"/>
		<Item Name="LesoStatus.ctl" Type="VI" URL="../subVI/LesoStatus.ctl"/>
		<Item Name="Leso3Entity.ctl" Type="VI" URL="../subVI/Leso3Entity.ctl"/>
	</Item>
	<Item Name="leso3Close.vi" Type="VI" URL="../subVI/leso3Close.vi"/>
	<Item Name="leso3FirmwareUpdate.vi" Type="VI" URL="../subVI/leso3FirmwareUpdate.vi"/>
	<Item Name="leso3GetAdcValue.vi" Type="VI" URL="../subVI/leso3GetAdcValue.vi"/>
	<Item Name="leso3GetCalibrationFactor.vi" Type="VI" URL="../subVI/leso3GetCalibrationFactor.vi"/>
	<Item Name="leso3GetDriverVersion.vi" Type="VI" URL="../subVI/leso3GetDriverVersion.vi"/>
	<Item Name="leso3GetFirmwareVersion.vi" Type="VI" URL="../subVI/leso3GetFirmwareVersion.vi"/>
	<Item Name="leso3GetMeasuredValue.vi" Type="VI" URL="../subVI/leso3GetMeasuredValue.vi"/>
	<Item Name="leso3GetSerialPorts.vi" Type="VI" URL="../subVI/leso3GetSerialPorts.vi"/>
	<Item Name="leso3GetSerialPortsAll.vi" Type="VI" URL="../subVI/leso3GetSerialPortsAll.vi"/>
	<Item Name="leso3Open.vi" Type="VI" URL="../subVI/leso3Open.vi"/>
	<Item Name="leso3Reset.vi" Type="VI" URL="../subVI/leso3Reset.vi"/>
	<Item Name="leso3SetCalibrationFactor.vi" Type="VI" URL="../subVI/leso3SetCalibrationFactor.vi"/>
	<Item Name="leso3SetDac.vi" Type="VI" URL="../subVI/leso3SetDac.vi"/>
	<Item Name="leso3SetImpulsMode.vi" Type="VI" URL="../subVI/leso3SetImpulsMode.vi"/>
	<Item Name="leso3SetRange.vi" Type="VI" URL="../subVI/leso3SetRange.vi"/>
	<Item Name="leso3SetSourse.vi" Type="VI" URL="../subVI/leso3SetSourse.vi"/>
	<Item Name="leso3TestDevice.vi" Type="VI" URL="../subVI/leso3TestDevice.vi"/>
</Library>
